<?php
/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*

A classe LeagueTablr acompanha o score de cada jogador em uma liga. Depois de cada jogo, o score do jogador é salvo utilizando a função recordResult.

O Rank de jogar na liga é calculado utilizando a seguinte lógica:

1- O jogador com a pontuação mais alta fica em primeiro lugar. O jogador com a pontuação mais baixa fica em último.
2- Se dois jogadores estiverem empatados, o jogador que jogou menos jogos é melhor posicionado.
3- Se dois jogadores estiverem empatados na pontuação e no número de jogos disputados, então o jogador que foi o primeiro na lista de jogadores é classificado mais alto.


Implemente a função playerRank que retorna o jogador de uma posição escolhida do ranking.

Exemplo:

$table = new LeagueTable(array('Mike', 'Chris', 'Arnold'));
$table->recordResult('Mike', 2);
$table->recordResult('Mike', 3);
$table->recordResult('Arnold', 5);
$table->recordResult('Chris', 5);
echo $table->playerRank(1);


Todos os jogadores têm a mesma pontuação. No entanto, Arnold e Chris jogaram menos jogos do que Mike, e como Chris está acima de Arnold na lista de jogadores, ele está em primeiro lugar.

Portanto, o código acima deve exibir "Chris".


*/

class LeagueTable
{
	public function __construct($players)
    {
		$this->standings = array();
		foreach($players as $index => $p)
        {
			$this->standings[$p] = array
            (
                'index' => $index,
                'games_played' => 0, 
                'score' => 0
            );
        }
	}
		
	public function recordResult($player, $score)
    {
		$this->standings[$player]['games_played']++;
		$this->standings[$player]['score'] += $score;
	}
	
	public function playerRank($rank)
    {

        if($rank > count($this->standings) || $rank <= 0){
            return "Rank não encontrado";
        };

        // Separando as arrays
        // e mantendo o indice igual para comparação
        $ranking_index = array_column($this->standings, "index");
        $ranking_game_played = array_column($this->standings, "games_played");
        $ranking_score = array_column($this->standings, "score") ;
 

        //Organiza por ordem descrescente e mantem os indices
        arsort($ranking_score);

        $ranking = [];
        //Vai percorrer o array onde esta organizado os pontos
        foreach($ranking_score as $key => $value){
            
            //Verifica se a variavel ranking tem algum elemento
            //Caso sim
            if(count($ranking) > 0){
                
                //Aqui temos a chave de indice da array anterior para fazermos as comparaçoes
                $key_array_prev = count($ranking) - 1;
                $score_array_prev = implode("",array_values($ranking[$key_array_prev]));
                $key_score_prev = implode("",array_keys($ranking[$key_array_prev]));

                //Se a array que estamos percorrendo tem o mesmo valor que a array anterior
                //Caso sim
                if($value == $score_array_prev){

                    //Verifica se o elemento atual tem MENOS jogos que o anterior
                    //Se sim
                    if($ranking_game_played[$key] < $ranking_game_played[$key_score_prev]){
                        
                        //Armazena os dados da array anterior em uma variavel temp
                        $temp = $ranking[$key_array_prev];

                        //Retira a variavel anterior do $ranking
                        unset($ranking[$key_array_prev]);

                        //Adiciona a array que estamos percorrendo no $ranking e logo em seguida a $temp
                        array_push($ranking, [$key => $value], $temp);

                    //Verifica se o elemento atual tem MAIS jogos que o anterior
                    }elseif($ranking_game_played[$key] > $ranking_game_played[$key_score_prev]){
                        //Adiciona no final da array $ranking
                        array_push($ranking, [$key => $value]);

                    //Se nao for nenhum dos casos quer dizer que jogaram a mesma quantidade de jogos
                    }else{
                        if ($ranking_index[$key] < $ranking_index[$key_score_prev]) {

                            //Armazena os dados da array anterior em uma variavel temp
                            $temp = $ranking[$key_array_prev];

                            //Retira a variavel anterior do $ranking
                            unset($ranking[$key_array_prev]);

                            //Adiciona a array que estamos percorrendo no $ranking e logo em seguida a $temp
                            array_push($ranking, [$key => $value], $temp);

                        }else{
                            array_push($ranking, [$key => $value]);
                        }

                    }


                //Caso não, adiciona o elemento percorrido no final
                }else{
                    array_push($ranking, [$key => $value]);
                }


            //Caso não, adiciona o elemento em que estamos percorrendo em uma nova array
            }else{
                array_push($ranking, [$key => $value]);
            }

            //Reseta as keys da array
            $ranking = array_values($ranking);

        }; //Fim Foreach
        


        //Retira -1 do rank definido pelo usuario
        //Para que os valores coincidem com os da array
        $rank -= 1;

        //Pega o primeiro indice do $ranking_score ja organizado em descrescente
            $key_ranking = implode(array_keys($ranking[$rank])) ;

        //Faz um foreach para percorrer todo o objeto
        foreach($this->standings as $name => $standing){

            // Compara com o index  | Procura qual index tem o mesmo 
            //      do objeto       |    indice que o do $key_ranking
            if($standing["index"] == $ranking_index[$key_ranking]){

                //Armazena o objeto comparado a variavel $ranking
                $ranking = $name;
                
            }
        }
      
        
        //Return
        return $ranking;
	}
}
      
$table = new LeagueTable(array('Mike', 'Chris', 'Arnold'));
$table->recordResult('Mike', 2);
$table->recordResult('Mike', 3);
$table->recordResult('Arnold', 5);
$table->recordResult('Chris', 5);

echo $table->playerRank(2);

