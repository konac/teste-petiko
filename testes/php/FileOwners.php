<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Implemente uma função que ao receber um array associativo contendo arquivos e donos, retorne os arquivos agrupados por dono. 

Por exempolo, um array ["Input.txt" => "Jose", "Code.py" => "Joao", "Output.txt" => "Jose"] a função groupByOwners deveria retornar ["Jose" => ["Input.txt", "Output.txt"], "Joao" => ["Code.py"]].


*/

class FileOwners
{
    public static function groupByOwners($files)
    {
        $array_name = [];
        $array_file = [];
        
        // Foreach para percorrer a array
        foreach($files as $file => $name){

            //Verifica se o $name está na $array_name
            if(in_array(strtolower($name), $array_name)){

                // Caso ja exista um $name dentro da array
                // Ele irá pegar o indice em que está localizado o nome repetido
                $key_array_name = array_search(strtolower($name), $array_name); 

                // e com isso adicionar o $file, no indice correspondente
                array_push($array_file[$key_array_name], $file);
            }else{

                //Caso não esteja, adiciona o name e o file, nas arrays indicadas
                array_push($array_name, strtolower($name));
                array_push($array_file, array($file));
            }
        };

        //Com tudo organizado por indice passo o array_combine
        $array_groups = array_combine($array_name, $array_file);

        return $array_groups;
    }
}

$files = array
(
    "Input.txt" => "Jose",
    "Code.py" => "Joao",
    "Output.txt" => "Jose",
    "Click.js" => "Maria",
    "Out.php" => "maria"
);
var_dump(FileOwners::groupByOwners($files));