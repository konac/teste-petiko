<!DOCTYPE html>
<html lang="pt-bt">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Petiko</title>
</head>
<body>
    <ul>
        <label>Javascript:</label>
        <li>formatDate.js - OK</li>
        <li>imageGallery.js - OK</li>
        <li>removeProperty.js - OK</li>
    </ul>
    <ul>
        <label>PHP:</label>
        <li>FileOwners.php - OK</li>
        <li>LeagueTable.php - OK</li>
        <li>Palindrome.php - OK</li>
    </ul>
    <ul>
        <label>REST:</label>
        <li>CEP.php - OK</li>
    </ul>
    <ul>
        <label>SQL:</label>
        <li>Pets.txt - OK</li>
        <li>Students.txt - OK</li>
    </ul>

</body>
<script src="js/formatDate.js"></script>
<!-- <script src="js/imageGallery.js"></script> -->
<script src="js/removeProperty.js"></script>
</html>